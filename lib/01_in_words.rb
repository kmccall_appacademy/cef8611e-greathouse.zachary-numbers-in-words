class Fixnum
  def in_words

    if self == 0
      return "zero"
    end

    #hash for ones place
    ones = {
      '1' => "one",
      '2' => "two",
      '3' => "three",
      '4' => "four",
      '5' => "five",
      '6' => "six",
      '7' => "seven",
      '8' => "eight",
      '9' => "nine"
    }
    #hash for tens place
    tens = {
      '2' => "twenty",
      '3' => "thirty",
      '4' => "forty",
      '5' => "fifty",
      '6' => "sixty",
      '7' => "seventy",
      '8' => "eighty",
      '9' => "ninety",
      '10'=> "ten",
      '11'=> "eleven",
      '12'=> "twelve",
      '13'=> "thirteen",
      '14'=> "fourteen",
      '15'=> "fifteen",
      '16'=> "sixteen",
      '17'=> "seventeen",
      '18'=> "eighteen",
      '19'=> "nineteen"
    }
    #hash for hundreds place
    hundreds = {
      '0' => "",
      '1' => "one hundred",
      '2' => "two hundred",
      '3' => "three hundred",
      '4' => "four hundred",
      '5' => "five hundred",
      '6' => "six hundred",
      '7' => "seven hundred",
      '8' => "eight hundred",
      '9' => "nine hundred",
    }

    #convert number to string, then to an array
    num_array = self.to_s.split("")

    #force array to have a length of 15 (possible digits) to split
    #into 5 parts
    until num_array.length == 15
      num_array.unshift("")
    end

    part_one = num_array.slice(-3..-1).join("")     #hundreds
    part_two = num_array.slice(-6..-4).join("")     #thousands
    part_three = num_array.slice(-9..-7).join("")   #millions
    part_four = num_array.slice(-12..-10).join("")  #billions
    part_five = num_array.slice(-15..-13).join("")  #trillions

    parts = [part_five, part_four, part_three, part_two, part_one]
    num_words = []

    #iterate through parts changing nummbers to words and adding them
    #to the num_words array
    parts.each do |part|
      ones_place = ones[part[-1]]
      tens_place = tens[part[-2]]
      tens_place_teens = tens[part[-2..-1]]
      hundreds_place = hundreds[part[-3]]
      tens_ones = "#{tens_place} #{ones_place}"
      hundreds_ones = "#{hundreds_place} #{ones_place}"
      hundreds_tens_teens = "#{hundreds_place} #{tens_place_teens}"
      hundreds_tens = "#{hundreds_place} #{tens_place}"
      hundreds_tens_ones = "#{hundreds_place} #{tens_ones}"

      if part.length == 0
        num_words.push("")
      #first digit
      elsif part.length == 1
        num_words.push(ones[part])
      #second digit
      elsif part.length == 2
        if part[-2] == '1'
          num_words.push(tens_place_teens)
        elsif part[-1] == "0"
          num_words.push(tens_place)
        else
          num_words.push(tens_ones)
        end
      #third digit
      elsif part.length == 3
        if part.slice(-2..-1) == "00"
          num_words.push(hundreds_place)
        elsif part[-2] == '1'
          num_words.push(hundreds_tens_teens)
        else
          if part[-2] == "0"
            num_words.push(hundreds_ones)
          elsif part[-1] == "0"
            num_words.push(hundreds_tens)
          else
            num_words.push(hundreds_tens_ones)
          end
        end
      end
    end #end of iterating through parts

    #formatting thousands
    if part_two != "" && part_two != "000" && part_one != "000"
      num_words[-2] += " thousand "
    end
    if part_two != "" && part_two != "000" && part_one == "000"
      num_words[-2] += " thousand"
    end

    after_part_three = part_two + part_one
    #formatting millions
    if part_three != "" && part_three != "000" && after_part_three != "000000"
      num_words[-3] += " million "
    end
    if part_three != "" && part_three != "000" && after_part_three == "000000"
      num_words[-3] += " million"
    end


    after_part_four = part_three + part_two + part_one
    #formatting billions
    if part_four != "" && part_four != "000" && after_part_four != "000000000"
      num_words[-4] += " billion "
    end
    if part_four != "" && part_four != "000" && after_part_four == "000000000"
      num_words[-4] += " billion"
    end


    after_part_five = part_four + part_three + part_two + part_one
    #formatting trillions
    if part_five != "" && part_five != "000" && after_part_five != "000000000000"
      num_words[-5] += " trillion "
    end
    if part_five != "" && part_five != "000" && after_part_five == "000000000000"
      num_words[-5] += " trillion"
    end

    #add all the strings together and perform final formatting touch
    return_num = num_words.reduce(:+)
    return_num.gsub("  ", " ")
  end
end
